# Killing Floor HUD for Counter-Strike: Source

Version 0.1

- Menu background image
- Menu sounds
- Weapon sounds
- Full screen MOTD
- HUD colors

## Installation

Copy `cstrike` folder to your game folder.